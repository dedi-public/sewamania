import firebase from 'firebase/app'
import 'firebase/firestore'

// Initialize Firebase
var config = {
    apiKey: "AIzaSyACgvKZ3jSHeerAzrXJKthUVwAgDQnW2AI",
    authDomain: "sewamania-bced0.firebaseapp.com",
    databaseURL: "https://sewamania-bced0.firebaseio.com",
    projectId: "sewamania-bced0",
    storageBucket: "sewamania-bced0.appspot.com",
    messagingSenderId: "232347292349"
  };
  firebase.initializeApp(config);
  const db = firebase.firestore();
  
  db.settings({ timestampsInSnapshots: true });

  export default db;